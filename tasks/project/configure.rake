namespace :lado_direito do
  desc 'Configure LadoDireito lib to act on project'
  task :install do
    APPLICATION_PATH  = File.dirname(__FILE__) + '/../../../../' unless defined? APPLICATION_PATH
    LADO_DIREITO_PATH = File.dirname(__FILE__) + '/../../' unless defined? LADO_DIREITO_PATH

    # Configure all hooks
    Rake::Task["git:create_hooks"].invoke true

    # Copy settings file
    ## rubocop file: lib/lado_direito/config/.rubocop.yml => app root path
    IO.popen "cp #{LADO_DIREITO_PATH}config/.rubocop.yml #{APPLICATION_PATH}"
  end
end
