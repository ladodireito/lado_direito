Dir.glob(File.dirname(__FILE__) + '/../../lib/*.rb').each { |f| require f }

namespace :sqs do
  desc 'Clear queue and write getted content - Stay listen while true.'
  task :clear_queue, :queue_name do |t, args|
    aws   = LadoDireito::Aws.new
    queue = aws.get_queue args[:queue_name]
    queue.poll do |msg|
      puts "Dequeued #{msg.body}"
    end
  end

end
