# Code Coverage
require 'simplecov'
SimpleCov.start

require 'json'
require 'rspec'
require 'rspec/mocks'

Dir[File.dirname(__FILE__) + '/../lib/*.rb'].each { |file| require file }
