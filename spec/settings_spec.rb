require File.dirname(__FILE__) + '/spec_helper.rb'

describe LadoDireito do
  describe LadoDireito::Settings do
    describe :initialize do
      context 'When is invalid' do
        it 'Should raise error if file informed on file_path does not exists' do
          file_path = '/some/invalid/file_path.ext'

          expect { LadoDireito::Settings.new file_path }.to raise_error ArgumentError, 'File path does not exists'
        end

        it 'Should raise error if informed file is not an YAML'
      end

      context 'When is valid' do
        it 'Should return parsed YAML' do
          file_path = 'bla'
          File.should_receive(:exists?).with(file_path).and_return(true)
          File.should_receive(:open).with(file_path).and_return('foo')

          LadoDireito::Settings.new file_path
        end
      end
    end

    describe :get_section do
      context 'When is valid' do
        it 'Should return an section from content' do
          file_path = 'bla'
          File.should_receive(:exists?).with(file_path).and_return(true)
          File.should_receive(:open).with(file_path).and_return({ section1: { foo: :bar }, section2: { key2: :value2 } }.to_json)

          item    = LadoDireito::Settings.new file_path
          section = item.get_section('section2')

          section.should == { 'key2' => 'value2' }
        end
      end
    end
  end
end
