Dir.glob(File.dirname(__FILE__) + '/../../lib/*.rb').each { |f| require f }

def get_config_file
    # Get configs file
    configs_path  = File.dirname(__FILE__) + '/../../../../config/settings.yml'
    fail '<app>/config/settings.yml does not exists' unless File.exists? configs_path

    LadoDireito::Settings.new(configs_path)
end
