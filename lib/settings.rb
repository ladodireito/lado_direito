# encoding: utf-8

require 'yaml'

# All functionallities from LadoDireito.
module LadoDireito
  attr_accessor :content

  # Get informations about a config file.
  #
  # author - Rodrigo Otavio van den Berg Maia <rodrigo@ladodireito.com>
  # date - 06/08/2013
  class Settings
    # Define to @content the informed file parsed.
    #
    # file_path - The full path of file to be parsed
    #
    # Returns the instance of class.
    def initialize(file_path)
      raise ArgumentError.new 'File path does not exists' unless File.exists? file_path

      @content  = YAML.load File.open file_path

      self
    end

    # Alias to section.
    # Section will be deprecated.
    def get_section(section_name)
      puts "ALERT: LadoDireito::Settings.get_section is deprecated. Use LadoDireito::Settings.section."

      section section_name
    end

    # Get an informed section from parsed config file.
    #
    # section_name - the section name.
    #
    # Return the section or nil if not found.
    def section(section_name)
      return nil unless @content.key? section_name

      @content[section_name]
    end
  end
end
