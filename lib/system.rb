# encoding: utf-8

require 'socket'

module LadoDireito
  # Run system commands.
  class System
    # Run a shell command and return the response.
    #
    # command - Full command to be executed.
    #
    # Return the response of exec command.
    def self.run_shell_command(command)
      output = IO.popen command

      response  = output.readlines
      output.close
      return response
    end

    # Get the hostname from computer
    #
    # Return the system hostname.
    def self.hostname
      Socket.gethostname
    end

    # Return the current Proccess ID
    #
    # Return the PID.
    def self.pid
      Process.pid
    end

    # returns the system architecture
    def self.architecture
      self.run_shell_command('uname -i').first.strip
    end
  end
end
