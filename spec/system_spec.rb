require File.dirname(__FILE__) + '/spec_helper.rb'

describe LadoDireito do
  describe LadoDireito::Settings do
    describe '#run_shell_command' do
      it 'Should run a command and return response' do
        hostname = IO.popen('hostname').readlines

        LadoDireito::System.run_shell_command('hostname').should eql(hostname)
      end
    end

    describe '#hostname' do
      it 'Should get machine hostname' do
        hostname = IO.popen('hostname').readlines.first.gsub(/\n/, '')

        LadoDireito::System.hostname.should eql(hostname)
      end
    end

    describe '#pid' do
      it 'Should get current PID' do

        Process.should_receive(:pid).once.and_return('foo')
        LadoDireito::System.pid.should eql('foo')
      end
    end

    describe '#architecture' do
      context 'when system is i686' do
        it 'gets the system architecture' do
          LadoDireito::System.should_receive(:run_shell_command).with('uname -i').once.and_return(["i686\n"])

          expect(LadoDireito::System.architecture).to eq('i686')
        end
      end

      context 'when system is x86_64' do
        it 'gets the system architecture' do
          LadoDireito::System.should_receive(:run_shell_command).with('uname -i').once.and_return(["x86_64\n"])

          expect(LadoDireito::System.architecture).to eq('x86_64')
        end
      end
    end
  end
end
