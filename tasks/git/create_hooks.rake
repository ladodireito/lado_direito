namespace :git do
  task :create_hooks, :overwrite do |task, attrs|
    overwrite = (attrs[:overwrite] == 'true' or attrs[:overwrite] == true)

    # Help message flag
    msg       = false

    APPLICATION_PATH  = File.dirname(__FILE__) + '/../../../../' unless defined? APPLICATION_PATH
    SAMPLE_HOOKS_DIR  = APPLICATION_PATH + 'lib/lado_direito/hooks/'
    CURRENT_HOOKS_DIR = APPLICATION_PATH + '.git/hooks/'

    # Get hook samples
    Dir[ SAMPLE_HOOKS_DIR + '*' ].each do |new_hook|
      # Verify if current hook already exist.
      # If exist, show warning message.
      # If not exist, copy current hook to repository hooks dir.
      hook_name = File.basename new_hook
      dest_file = CURRENT_HOOKS_DIR + hook_name

      if File.exists? dest_file and !overwrite
        msg = true
        puts "WARNING: #{hook_name} Already exists and can not overwrite."
      else
        # Copy file to dest dir.
        IO.popen "cp #{new_hook} #{dest_file}"

        # Change file permissions to executable.
        IO.popen "chmod +x #{dest_file}"

        puts "#{hook_name} successfully added to project hooks!"
      end

      if msg
        puts "\n"
        puts "If you want to overwrite current existent hooks, use rake git:create_hooks overwrite=true"
      end
    end
  end
end
