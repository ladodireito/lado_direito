# Debug things
#
# attrs - Array (or a single) of attributes to be debugged
def debug(attrs = [])
  puts "\n\n"
  if attrs.is_a? Array
    attrs.each do |attr|
      puts '#' * 170
      p attr
    end
  else
    puts '#' * 170
    p attrs
  end
  puts '#' * 170
  puts "\n\n"
end
