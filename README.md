# Including on your project

    $ mkdir <your-project-root>/lib
    $ git submodule add git@bitbucket.org:ladodireito/lado_direito.git lib/lado_direito

# Dependencies
Add the follow gems to Gemfile (if is not already added):

    # <your_project_root>/Gemfile
    source 'https://rubygems.org'
    
    gem 'rspec'
    gem 'rdoc'
    gem 'rubocop'
    gem 'simplecov'

and install all of them:

    $ bundle install

# Functionalities

## Rake tasks
Add the following line in the begin of your Rakefile. If you do not have a Rakefile, create one with the follow content:

    # <your_project_root>/Rakefile

    # Include lado_direito lib tasks.
    Dir.glob('./lib/lado_direito/tasks/*/*.rake').each { |r| import r }

## Install
Type

    $ rake lado_direito:install

## Imap read

    # you_code.rb
    require './lib/lado_direito/imap.rb'
    require './lib/lado_direito/imap_message.rb'
    
    # Params to stabilish connection. You can parse an yml file to.
    params  = {server_name: 'my imap server', server_port: 'port', server_username: 'username', server_password: 'password'}
    
    # Stablish connection with imap server.
    imap  = LadoDireito::Imap.new params
    
    # Get all message ids from INBOX folder.
    message_ids = imap.all_messages_from_folder('INBOX')
    
    # Get a message with ID 1 from INBOX folder.
    message = imap.get_message(1, 'INBOX')
    
    # Get subject from message
    subject = message.subject
    
    # Get body content from message
    content = message.body_message

