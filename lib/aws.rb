require 'aws-sdk'

module LadoDireito
  # Class to access AWS.
  class Aws
    attr_accessor :sqs, :s3, :configs

    def initialize(file_path = nil)
      file_path   = File.dirname(File.expand_path(__FILE__ + '/../')) + '/config/settings.yml' if file_path.nil?
      config_file = File.open file_path
      configs     = YAML.load config_file
      config_file.close

      @configs    = configs[:aws]
    end

    # Getter, lazy load.
    #
    # Return AWS::SQS instance
    def sqs
      @sqs = AWS::SQS.new(access_key_id: configs[:aws_access_key_id], secret_access_key: configs[:aws_secret_access_key]) if @sqs.nil?

      @sqs
    end

    # Create or get an existing queue by queue_name.
    #
    # queue_name - Queue name.
    #
    # Return instance of queue.
    def get_queue(queue_name)
      raise ArgumentError.new 'queue_name must be a string' unless queue_name.kind_of? String
      raise ArgumentError.new 'queue_name can not be empty' if queue_name.empty?

      sqs.queues.create queue_name
    end

    # Put an message in a queue.
    #
    # queue_name - Queue name.
    # message - message to store on queue_name.
    #
    # Return operation response id.
    def put_sqs(queue_name, message)
      raise ArgumentError.new 'queue_name must be a string' unless queue_name.kind_of? String
      raise ArgumentError.new 'queue_name can not be empty' if queue_name.empty?
      raise ArgumentError.new 'message must be a string' unless message.kind_of? String
      raise ArgumentError.new 'message can not be empty' if message.empty?

      queue = get_queue queue_name
      sent  = queue.send_message message

      sent.id
    end

    # AWS::S3 Lazy loader
    #
    # Return @return AWS::S3 instance
    def s3
      # need if @s3.nil at eol ? - jdg
      AWS.config access_key_id: configs[:aws_access_key_id], secret_access_key: configs[:aws_secret_access_key]
      @s3 = AWS::S3.new

      @s3
    end

    # Store a file in s3 bucket.
    #
    # bucket_name           - Bucket to store file
    # origin_file_path      - Full path of origin file
    # destination_file_path - Name of dest file, if is different of original.
    def send_s3(bucket_name, origin_file_path, destination_file_path = nil)
      destination_file_path = File.basename(origin_file_path) if destination_file_path.nil?

      s3.buckets[bucket_name].objects[destination_file_path].write file: origin_file_path
    end

    # Gets a bucket from S3 Server
    #
    # bucket_name - Bucket name
    #
    # Return S3.Bucket
    def get_s3_bucket(bucket_name)
      raise ArgumentError.new 'bucket_name must be a string' unless bucket_name.kind_of? String
      raise ArgumentError.new 'bucket_name can not be empty' if bucket_name.empty?

      s3.buckets[bucket_name]
    end
  end
end
